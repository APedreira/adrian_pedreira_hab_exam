/*
* * * * * * * * * * * * * * * * * * * * *
*  2. D A D O   E L E C T R Ãƒâ€œ N I C O   *
* * * * * * * * * * * * * * * * * * * * *
 
Simula el uso de un dado electrÃƒÂ³nico cuyos valores al azar irÃƒÂ¡n del 1 al 6. 

    - Crea una variable "totalScore" en la que se irÃƒÂ¡ almacenando la puntuaciÃƒÂ³n total tras cada una de las tiradas. 

    - Una vez alcanzados los 50 puntos el programa se detendrÃƒÂ¡ y se mostrarÃƒÂ¡ un mensaje que indique el fin de la partida.

    - Debes mostrar por pantalla los distintos valores que nos devuelva el dado (nÃƒÂmeros del 1 al 6) asÃƒÂ­ como el valor de la
      variable "totalScore" tras cada una de las tiradas. 
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
*/


function dicesGame(threshold) {
    let totalScore = []
    let throws = 1
    for (let accum = 0; accum <= threshold; throws++) {
        let randomNumber = (Math.round(Math.random() * (6 - 1) + 1))
        accum += randomNumber
        totalScore.push({ "Lanzamiento nº": throws, "Puntuación": randomNumber, "Puntuación total": accum })
    }
    console.log(`***************** FIN DE LA PARTIDA *******************`)
    console.log(`Has logrado alcanzar los ${threshold} puntos tras ${throws - 1} lanzamientos`)
    console.log(totalScore)
}

dicesGame(50)